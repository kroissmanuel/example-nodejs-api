var exampleService = require('../service/exampleService');

exports.getItems = function (req, res) {
    return res.json(exampleService.getItems());
};