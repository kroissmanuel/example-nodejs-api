'use strict';
module.exports = function (app) {
    var exampleController = require('../controllers/exampleController');
    var express = require('express');

    var router = express.Router();

    router.get('/example', exampleController.getItems);

    app.use('/api', router);

};