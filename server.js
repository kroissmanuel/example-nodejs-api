var express = require('express');
var cors = require('cors');


app = express();
app.use(cors());
port = process.env.PORT || 3000;
require('./api/routes/exampleRoutes')(app);

app.listen(port);



console.log('Example RESTful API server started on: ' + port);